
import combinator as con
import json
import math
import adminTool
import firestore_connent as FS
from datetime import timedelta
print("Processing:")


def sepPlaceDB():
    with open("scorch-real-hk-export.json", 'r', encoding="utf-8") as alter:
        alternative = json.load(alter)["bundled"]["place"]
        print(alternative)
        adminTool.contentToFirebase(
            source="scorch-real-hk-export.json",
            endpoint="https://scorch-content.firebaseio.com/package/hkg/place.json", alternative=alternative)


def sepCounterDB():
    with open("scorch-real-hk-export.json", 'r', encoding="utf-8") as alter:
        alternative = json.load(alter)["bundled"]["labelCounter"]
        # json.load(alter)["bundled"]["placeCounter"]

        print(alternative)
        adminTool.contentToFirebase(
            source="updates/place_Type.json",
            endpoint="https://scorch-config.firebaseio.com/package/global/labelCounter.json", alternative=alternative)


def sepLabelDB(source: str):
    alternative: dict = {}
    exportContent: dict = {}
    with open(source, 'r', encoding="utf-8") as alter:
        alternative = json.load(alter)["data"]
    line = 1
    for alterK, alterV in alternative.items():
        region: list = alterV.get("region")
        if (region == None):
            target: dict = exportContent.get("0")
            if(target == None):
                exportContent["0"] = {alterK: alterV}
            else:
                exportContent["0"][alterK] = alterV
        else:
            for specific in region:
                target: dict = exportContent.get(specific)
                if(target == None):
                    exportContent[specific] = {alterK: alterV}
                else:
                    exportContent[specific][alterK] = alterV

        line += 1

    processCount = 0
    for uK, uV in exportContent.items():
        count: int = len(uV)
        md: int = adminTool.timestamp()

        processCount += 1
        adminTool.contentToFirebase(
            source="", endpoint=f"https://scorch-content.firebaseio.com/package/{uK}/keyword.json", alternative={"count": count, "md": md, "data": uV})

    print(
        f"alternativeID:{uK}, totle content: {count}")


placeID: str = "ChIJ-xc9Kr52bjQR6YmrOUzDYf4"
# placeIDZhTest: str = "ChIJI2GNQeh2bjQRXjS4lHpyikM"
location: dict = {'lat': 22.329128, 'lng': 114.106841}
c = input("Please input the PW\n")
if c == "A781030A":
    print("PW vaild, do the job now")
    # sepLabelDB("v7Source/indexedLabelV7.json")
    # FS.bulkContentUpdate(
    #     jsonSource="v7Source/restructuredPlaceV8.json", endpoint="place")
    # adminTool.contentToFirebase(source="v7Source/restructuredPlaceV8.json",
    #                             endpoint="https://scorch-content.firebaseio.com/package/8867/place.json")
    # adminTool.bulkPlaceSearch(
    #     sourceCSV="v7Source/place.csv", outputJson="v7Source/restructuredPlaceV8.json")

    adminTool.fetchPlaceByKeyword(pid="ChIJIaZVCnt2bjQRmUkFdb-ZH7w")
    # adminTool.storeImage(source="v7Source/restructuredPlaceV8.json")
    # for i in range(3):
    #     adminTool.placeComment(
    #         pid=placeID, output=f"comment-test/batch{i}.json")
else:
    print("PW Invaild, re-enter")
print("Job Done")
