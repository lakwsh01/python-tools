
import json
import csv
import requests
import staticKey
import firebase_admin
import time
import combinator


def contentToFS(source: str, endpoint: str, alternative: dict = None):
    if alternative == None:
        alternative = readJson(source)
    else:
        pass


def contentToFirebase(source: str, endpoint: str, alternative: dict = None):
    jsonData: str
    if alternative == None:
        with open(source, 'r', encoding="utf-8") as data:
            jsonData = json.load(data)

            print(jsonData)
            res = requests.put(url=endpoint, json=jsonData)
            print(res.status_code)
            print(res.text)

    else:
        jsonData = json.dumps(alternative)
        print(jsonData)
        res = requests.put(url=endpoint, data=jsonData)
        print(res.status_code)
        print(res.text)


def labelTypeChecker(asset: dict, checker: int):
    if asset['labelType'] == int:
        return True
    else:
        return False


def countObject(source: str):
    with open(source, 'r', encoding="utf-8") as data:
        content = json.load(data)
        print(f"counter:{len(content)}")


def placeComment(pid: str, output: str):
    endpointSub: str = f"https://maps.googleapis.com/maps/api/place/details/json?place_id={pid}&language=zh&fields=reviews&key={staticKey.mapAPIKey}"
    jsonRes = requests.get(url=endpointSub).json()
    subContent = jsonRes.get('result')
    # print(f"content: {subContent}")
    writeJson(data=subContent, source=output)
    print("get comments from google, save to ", output)
    return subContent


def bulkPlaceSearch(sourceCSV: str, outputJson: str, labelVersion: str = "7"):
    places: dict = dict({'md': 0, 'count': 0, 'data': {}})
    labelMap: dict
    labelSource: dict = {"7": "v7Source/indexedLabelV7.json"}
    if outputJson != None:
        with open(labelSource.get(labelVersion, labelSource["7"]), 'r', encoding='utf-8') as labels:
            labelMap = json.load(labels)["data"]

        with open(sourceCSV, 'r', encoding='utf-8') as source:

            line = 0
            spamreader = csv.reader(source)
            for row in spamreader:
                if line == 0:
                    print(f'column: {row}')

                else:

                    print(
                        f"line {line},  {row[3]} is processing")
                    placeDetail: dict = dict({
                        'indicator': {
                            'source': "google_place_api",

                        },
                        'md': timestamp(),
                    })

                    placeDetail['certificate'] = combinator.certificateGenerator(
                        labeled=row[6], labels=labelMap, issuer=combinator.certIssuer.get(row[0], "Scorch.Team"))
                    getPlaceDetailByGooglePlaceAPI(placeID=row[3])

                    placeDetail['details']['coverImage'] = row[5]
                    if (placeDetail['details'].get('phone') == None):
                        placeDetail['details']['phone'] = row[4]
                    places['data'][row[3]] = placeDetail

                line += 1

                def getPlaceDetailByGooglePlaceAPI(placeID: str):
                    endpointENUS: str = f"https://maps.googleapis.com/maps/api/place/details/json?place_id={placeID}&language=en&fields=name,geometry,formatted_address,opening_hours,types,formatted_phone_number,price_level,website,rating,user_ratings_total&key={staticKey.mapAPIKey}"
                    endpointZH: str = f"https://maps.googleapis.com/maps/api/place/details/json?place_id={placeID}&language=zh_tw&fields=name,formatted_address&key={staticKey.mapAPIKey}"
                    endpointZHHK: str = f"https://maps.googleapis.com/maps/api/place/details/json?place_id={placeID}&language=zh_hk&fields=name,formatted_address&key={staticKey.mapAPIKey}"

                    resENUS = requests.get(url=endpointENUS)
                    resZH = requests.get(url=endpointZH)

                    mainContent: dict = resENUS.json()
                    subContent: dict = resZH.json()
                    if resZH == None:
                        subContent = requests.get(url=endpointZHHK).json()
                    # indicator: {
                    #   terms : [] (from autocomplete)
                    #   description: '' (from autocomplete)
                    #   region: '' (if address contain Hong Kong, Taiwan / LatLng Check)
                    #   secondaryDescription: ''(from autocomplete)
                    # }
                    # locale: {
                    #   en:{
                    #       address:'',
                    #       title:''
                    #       },
                    #   zh:{
                    #       address:'',
                    #       title:''
                    #       }
                    #
                    # }
                    # opening_hours:
                    #   ['opening_hours']['periods']
                    #

                    placeDetail['indicator'] = {
                        'region': regionGetter(address=mainContent['result']['formatted_address'], location=mainContent['result']['geometry']['location']),
                        'description': mainContent['result'].get('description'),
                        'terms': mainContent['result'].get('terms'),
                        'secondaryDescription': mainContent['result'].get('secondaryDescription'),
                        'source': 'google_place_api',
                        'types': mainContent['result'].get('types'),
                        'location': mainContent['result']['geometry']['location'],
                        'viewport': mainContent['result']['geometry']['viewport']

                    }
                    opening_hours = mainContent['result'].get(
                        'opening_hours')

                    readable: list
                    perioidic: list

                    if opening_hours != None:

                        perioidic = combinator.timeToPeriordic(
                            opening_hours["periods"])
                        readable = opening_hours["weekday_text"]
                    else:
                        readable = None
                        perioidic = None

                    placeDetail['locale'] = {
                        'en_us': {
                            'address': mainContent['result']['formatted_address'],
                            'title': mainContent['result']['name']
                        },
                        'zh_tw': {
                            'address': subContent['result']['formatted_address'],
                            'title': subContent['result']['name']
                        },
                        'zh_hk': {
                            'address': subContent['result']['formatted_address'],
                            'title': subContent['result']['name']
                        }
                    }
                    placeDetail['details'] = {
                        'price_level': mainContent['result'].get('price_level'),
                        'phone': mainContent['result'].get('formatted_phone_number'),
                        'website': mainContent['result'].get("website"),
                        "rating": mainContent['result'].get("rating"),
                        "rated": mainContent['result'].get('user_ratings_total'),
                        "opening_hour": readable,
                        "perioidic": perioidic
                    }
                    return placeDetail
                    # def bulkLabelGeneration(label:dict):

                def regionGetter(address: str, location: dict):
                    address = address.lower()
                    if ('hong kong' in address) | ('香港' in address):
                        return '852'
                    elif ('tainan' in address) | ('台南' in address) | ('臺南' in address):
                        return '8867'
                    else:
                        with open('v7Source/region.json', 'r', encoding='utf-8') as region:
                            latlngBounds: dict = json.load(region)
                            for rK, rV in latlngBounds.items():
                                latlngBound: dict = rV.get('latlngbound')
                                if (latlngBound['northeast']['lat'] > location['lat'] > latlngBound['southwest']['lat']) & (latlngBound['northeast']['lng'] > location['lng'] > latlngBound['southwest']['lng']):
                                    return rK
                                else:
                                    pass
                            return None

        count: int = len(places['data'])
        places['md'] = timestamp()
        places['count'] = count
        with open(outputJson, 'w', encoding='utf-8') as output:
            json.dump(places, output, ensure_ascii=False)
    else:
        print("Please Specify A output File, Otherwise it would help you")
        pass


def labelTypeSpecific(sourceCSV: str, outputJSON: str):
    with open(sourceCSV, 'r', encoding='utf-8') as source:
        spamreader = csv.reader(source)

        labels: dict = readJson(source="updates/labelV4.json")["data"]

        line = 0
        for row in spamreader:
            if line == 0:
                print(f'column: {row}')

            else:
                tempID: str = (row[0].split("<")[1]).split(">")[0]
                keyword: str = (row[0].split("<"))[0]
                labelID: str = combinator.labelIDLookup(label=keyword,
                                                        labels=labels, tempID=tempID)
                print(f"labelID: {labelID}, tempID: {tempID}")

            line += 1


def getPlaceDetail(pid: str):
    endpointSub: str = f"https://maps.googleapis.com/maps/api/place/details/json?place_id={pid}&language=en&fields=name,geometry,formatted_address,opening_hours,types,formatted_phone_number,price_level,website,rating,user_ratings_total&key={staticKey.mapAPIKey}"
    jsonRes = requests.get(url=endpointSub).json()
    subContent = jsonRes.get('result')
    print(f"content: {subContent}")
    return subContent


def fetchPlaceByKeyword(pid: str):

    endpointSub: str = f"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.9939271,120.2021664&radius=5000&type=restaurant&keyword=寵物友善&key={staticKey.mapAPIKey}"
    jsonRes = requests.get(url=endpointSub).json()
    subContent = jsonRes.get('results')
    # print(f"content: {subContent}")

    writeJson(source="google_place_api_test/place_search.json",
              data=jsonRes, formating=False)
    return subContent


def restructureOutdatedPlace(jsonSource: str, jsonOutput: str, alternative: dict = None, requestUpdate: bool = False):
    placesContent: dict = dict()
    if alternative == None:
        placesContent = readJson(source=jsonSource)["bundled"]['place']['data']
    else:
        placesContent = alternative

    def getPlaceSupplymentory(placeID: str, original: dict):
        endpointSub: str = f"https://maps.googleapis.com/maps/api/place/details/json?place_id={placeID}&language=en&fields=opening_hours,formatted_phone_number,price_level,website,rating,user_ratings_total&key={staticKey.mapAPIKey}"
        jsonRes = requests.get(url=endpointSub).json()
        status: str = jsonRes.get('status')
        subContent = jsonRes.get('result')

        if subContent != None:
            opening_hours = subContent.get(
                'opening_hours')
            if opening_hours != None:
                original['opening_hours'] = opening_hours['periods']
            else:
                original['opening_hours'] = None

            original['details'] = {
                'price_level': subContent.get('price_level'),
                'phone': subContent.get('formatted_phone_number'),
                'website': subContent.get("website"),
                "rating": subContent.get("rating"),
                "rated": subContent.get('user_ratings_total')
            }

            original["indicator"]['region'] = 'hkg'
            original["indicator"]['source'] = "google_place_api"

            secondaryDescription: str = original["indicator"].get(
                "secondaryDescription")
            terms: list = original["indicator"].get("terms")

            if secondaryDescription == None:
                original["indicator"]["secondaryDescription"] = None
            else:
                pass
            if (terms == None):
                original["indicator"]["terms"] = None
            elif (len(terms) == 0):
                original["indicator"]["terms"] = None
            else:
                pass
            if original["indicator"]["region"] == "hkg":
                vps: dict = readJson("region.json")[
                    "hkg"]["viewport"]["viewports"]
                vp: str = defineVP(
                    address=original["locale"]["en"]["address"], vps=vps)
                if vp != None:
                    original["indicator"]["viewport"] = vp
                else:
                    pass
            else:
                pass

        else:
            pass

        if status == "OK":
            original["status"] = "onStage"
        elif status == "NOT_FOUND":
            original["status"] = "withdrawn"
        else:
            original["status"] = status

        return original

    count = len(placesContent)
    md = timestamp()
    contentCount: int = 1
    for pK, pV in placesContent.items():
        if requestUpdate == True:
            placesContent[pK] = getPlaceSupplymentory(pK, pV)
            status: str = placesContent.get("status")
        else:
            status: str = placesContent.get("status")
        print(f"{pK} Handled, Status: {status}, processing: {contentCount}/ {count}")

        contentCount += 1
    writeJson(source=jsonOutput, data={
              'data': placesContent, 'count': count, 'md': md})


def defineVP(address: str, vps: dict):
    for vpK, vpV in vps.items():
        if (vpV["locale"]["en"] in address) | (vpV["locale"]["zh"] in address):
            return vpK
        else:
            pass


def timestamp():
    ss = time.time()
    return int((ss)*1000)


def readJson(source: str):
    with open(source, 'r', encoding='utf-8') as content:
        return json.load(content)


def writeJson(source: str, data: dict, formating: bool = True):
    with open(source, 'w', encoding='utf-8') as content:
        formated: dict
        if formating:
            formated: dict = {"count": len(
                data), "md": timestamp(), "data": data}
        else:
            formated = data
        json.dump(formated, content, ensure_ascii=False)


def place2RTDBV4(source: str, debug: bool = True):
    endpoint: str
    if (debug):
        endpoint = "https://scorch-test.firebaseio.com/"
    else:
        endpoint = "https://scorch-config.firebaseio.com/"
    content: dict = readJson(source)["data"]
    placeHK: dict = dict()
    placeTW: dict = dict()

    line = 1
    count = len(content)
    for pK, pV in content.items():
        region = pV["indicator"]["region"]
        if region == "852":
            placeHK[pK] = pV
            print(
                f"{pK} added to PlaceHK, place count: {len(placeHK)}, line: {line} / {count} ")
        else:
            placeTW[pK] = pV
            print(
                f"{pK} added to PlaceTW, place count: {len(placeTW)}, line: {line} / {count}")

        line += 1

    resHK = requests.put(
        url=f"{endpoint}package/852/place.json", data=json.dumps(
            {
                "md": timestamp(),
                "data": placeHK,
                "count": len(placeHK)
            }))

    resTW = requests.put(
        f"{endpoint}package/8866/place.json", data=json.dumps({
            "md": timestamp(),
            "data": placeTW,
            "count": len(placeTW)
        }))

    print(
        f"HKDB status: {resHK.status_code}, HKDB status: {resTW.status_code}")


def deleteFromRTDB(endpoint):
    res = requests.delete(endpoint)
    print(f"Delete Job Status: {res}")


# def placeConvertor(source: str, output: str, version: int):

#     def version7Convertor(rowContent: list):
#         id: str = rowContent[3]
#         suppumentaryContent: getPlaceDetail(pid=id)
#         content: dict = {
#             "indicator": "",
#             "certificate": "",
#             "details": "",
#             "locale": "",
#         }

#         return print("Hello World")

#     placeContent: dict = {}
#     with open(source, 'r', encoding="utf-8") as csvfile:
#         sourcesContent = csv.reader(csvfile, delimiter=',')
#         line_count = 0
#         for row in sourcesContent:
#             if line_count == 0:
#                 print(f'Column names are {",".join(row)}')
#             elif row[1] == "":
#                 print(f'Row {line_count} is empty, Return')
#             else:
#                 print(f'Row {line_count} : {row}')
#                 id: str = row[3]
#                 placeContent[id] = "hello World"
#             line_count += 1

#     versionConvertor: dict = {7: version7Convertor()
#                               }

#     print(placeContent)
#     return versionConvertor.get(version, print("Version not find"))


def storeImage(source: str, outPutPath: str = "v7Source/image"):
    placeContent = readJson(source)["data"]
    line: int = 0
    _pV: dict
    _pK: str
    for _pK, _pV in placeContent.items():
        image: str = _pV["details"].get("coverImage")

        print(f"processing {line} / {len(placeContent)} WITH ID: {_pK}")
        if (image == None):
            print(f"{_pK} have no coverImage")
        elif image.endswith(".com"):
            print(f"{_pK} with a unreadable link: {image}")
        else:
            try:
                end: str = "jpg"
                r = requests.get(image)
                with open(outPutPath+"/"+_pK+"."+end, 'wb') as outfile:
                    outfile.write(r.content)

                # headers = {
                #     'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3'}

                # req = Request(url=image, headers=headers)
                # html = urlopen(req).read()

                # urlopen.request.urlretrieve(
                #     image, outPutPath+"/"+_pK+"."+end)
                # print(f"{_pK} have got the image")
            except requests.exceptions.HTTPError as err:
                raise SystemExit(err)
        line += 1
