import pyqrcode

from PIL import Image
datas: list = [
    "https://scorch.tech/share/place/bsWH",
    "https://scorch.tech/share/place/4zHd",
    "https://scorch.tech/share/place/2foq",
    "https://scorch.tech/share/place/RLd2",
    "https://scorch.tech/share/place/C1T9",
    "https://scorch.tech/share/place/hbc6",
    "https://scorch.tech/share/place/z44o",
    "https://scorch.tech/share/place/Ym6H",
    "https://scorch.tech/share/place/4M9Y",
    "https://scorch.tech/share/place/FtTi",
    "https://scorch.tech/share/place/SBja",
    "https://scorch.tech/share/place/VVRF",
    "https://scorch.tech/share/place/jfPk",
    "https://scorch.tech/share/place/m9Mm",
    "https://scorch.tech/share/place/WSz2",
    "https://scorch.tech/share/place/q5zc",
    "https://scorch.tech/share/place/eQb2",
    "https://scorch.tech/share/place/kjzH",
    "https://scorch.tech/share/place/Xj62",
    "https://scorch.tech/share/place/4gJH",
    "https://scorch.tech/share/place/8r2P",
    "https://scorch.tech/share/place/K513",
    "https://scorch.tech/share/place/CTck",
    "https://scorch.tech/share/place/gpdR",
    "https://scorch.tech/share/place/iPUX",
    "https://scorch.tech/share/place/L6JA",
    "https://scorch.tech/share/place/qLo3",
    "https://scorch.tech/share/place/AvmB",
    "https://scorch.tech/share/place/AvmB",
    "https://scorch.tech/share/place/uy1b",
    "https://scorch.tech/share/place/3N7P",
    "https://scorch.tech/share/place/DQbR",
    "https://scorch.tech/share/place/76UZ"
]


# Generate the qr code and save as png
for link in datas:

    filePath = link.split("/")
    lastIndex = len(filePath) - 1
    fileName = f"share_place_{filePath[lastIndex]}"

    qrobj = pyqrcode.create(link)
    with open(f'qr_code/0709/qr_code/original/{fileName}.png', 'wb') as f:
        qrobj.png(f, scale=10, background=(255, 255, 255, 255))

    # Now open that png image to put the logo
    img = Image.open(f'qr_code/0709/qr_code/original/{fileName}.png')
    width, height = img.size

    img = img.convert("RGBA")
# How big the logo we want to put in the qr code png
    logo_size = 100

# Open the logo image
    logo = Image.open('assets/logo/2x/w-2x.jpg')

# Calculate xmin, ymin, xmax, ymax to put the logo
    xmin = ymin = int((width / 2) - (logo_size / 2))
    xmax = ymax = int((width / 2) + (logo_size / 2))

# resize the logo as calculated

# put the logo in the qr code

    box = (150, 150, 300, 300)
    img.crop(box)
    region = logo
    region = region.resize((box[2] - box[0], box[3] - box[1]))
    img.paste(region, box)

    img.save(f"qr_code/0709/qr_code/with_logo/{fileName}.png")
    print(f"{fileName}.png saved")

# def im_crop_around(img, xc, yc, w, h):
#     img_width, img_height = img.size  # Get dimensions
#     left, right = xc - w / 2, xc + w / 2
#     top, bottom = yc - h / 2, yc + h / 2
#     left, top = round(max(0, left)), round(max(0, top))
#     right, bottom = round(min(img_width - 0, right)), round(min(img_height - 0, bottom))
#     return img.crop((left, top, right, bottom))


# def im_crop_center(img, w, h):
#     img_width, img_height = img.size
#     left, right = (img_width - w) / 2, (img_width + w) / 2
#     top, bottom = (img_height - h) / 2, (img_height + h) / 2
#     left, top = round(max(0, left)), round(max(0, top))
#     right, bottom = round(min(img_width - 0, right)), round(min(img_height - 0, bottom))
#     return img.crop((left, top, right, bottom))


# im = Image.open("qr_code/0709/qr_code/test.png")
# width, height = im.size   # Get dimensions
# print("content width: ",width, " content height: ", height)
# new_width, new_height=  450,450
# left = (width - new_width)/2
# top = (height - new_height)/2
# right = (width + new_width)/2
# bottom = (height + new_height)/2

# # Crop the center of the image
# # im = im_crop_around(img=im,xc=width,yc = new_height, w=new_width, h=new_height)

# image_data = im.load()

# for loop1 in range(height):
#     for loop2 in range(width):
#         r,g,b = im[loop1,loop2]
#         image_data[loop1,loop2] = 0,0,0
# im.save('changed.jpeg')
