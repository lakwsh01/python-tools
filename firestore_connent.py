import adminTool as tool
import firebase_admin
import google.cloud
from google.cloud import firestore
import datetime
from time import sleep
import google.cloud.exceptions
import json


def initDB():
    from firebase_admin import credentials, firestore
    cred = credentials.Certificate(
        "taglayer-99cfc-firebase-adminsdk-lzuk7-d6cb19e33f.json")
    firebase_admin.initialize_app(cred)
    return firestore.client()


db = initDB()


def bulkLabelGeneration(source: str, output: str = None, endpoint: str = "label", rewriteID=False):
    data: dict = dict()
    reWritedMap: dict = dict()
    with open(source, 'r', encoding="utf-8") as labels:
        data = json.load(labels)
    length: len(data)

    tx = db.transaction()
    i = 0

    @firestore.transactional
    def inTX(transaction, updates: dict):
        doc_ref = db.collection(endpoint).document()
        tx.set(doc_ref, updates)
        print(f'{doc_ref.id} updated')
        return doc_ref.id

    for subDataK, subDataV in data['data'].items():
        newID: str = inTX(tx, updates=subDataV)
        print(f'{newID} is replacing {subDataK} , {length} / {i}')
        if rewriteID:
            subDataK = newID
        reWritedMap[subDataK] = subDataV
        i += 1

    if output != None:
        with open(output, 'w', encoding='utf-8') as outputIO:
            json.dump(obj=reWritedMap, fp=outputIO, ensure_ascii=False)


# bulkLabelGeneration(source="labelContent.json",
#                     output="outputtest.json", endpoint="label", rewriteID=True)


def bulkContentUpdate(jsonSource: str, endpoint: str, alternative: dict = None):
    if alternative == None:
        jsonContent = tool.readJson(source=jsonSource)
    else:
        pass
    count = jsonContent["count"]
    tx = db.transaction()
    i: int = 1

    @firestore.transactional
    def inTX(transaction, updates: dict, key: str):
        ref_path = endpoint + "/" + key
        doc_ref = db.document(ref_path)
        tx.set(doc_ref, updates)
        print(f'{doc_ref.id} updated {i}/{count}')

    for uK, uV in jsonContent["data"].items():
        inTX(tx, updates=uV, key=uK)
        i += 1


def getDB(jsonOutput: str, path: str):
    userContent: dict = dict()
    snapshot = db.collection(f'{path}').stream()

    line = 1
    for snap in snapshot:
        userContent[snap.id] = snap.to_dict()
        print(f"Decoding {line}")
        line += 1

    tool.writeJson(source=jsonOutput, data=userContent)
