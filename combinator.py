from datetime import timedelta
import re
import json
import csv
import adminTool
from datetime import datetime as dtt

certIssuer: dict = {
    "亮諺": "scorch.ly",
    "靜慧": "scorch.jw",
    "奎鋒": "scorch.kf",
    "于婷": "scorch.yt",
    "嘉妏": "scorch.jm"

}

shouldRemoveKeyword: list = [
    "temp0009",
    "temp00180",
    "temp00181",
    "temp00025",
    "temp00027",
    "temp00187",
    "temp00090",
    "temp-i-00129",
    "temp-i-00148",
    "temp-i-00138",
    "temp-i-00141",
    "temp00178",
    "temp00104",
    "temp00105",
    "temp00107",
    "temp00111",
    "temp00112",
    "temp00114",
    "temp00115",
    "temp00118"
]


# def printHello(t: str):
#     print("Hello World", t)


# printHello("Paul")


# class Test(object):
#     def __init__(self, data):
#         self.__dict__ = json.loads(data)


# with open('taglayer-99cfc-export.json', 'r',encoding="utf-8") as f:
#     map_json = json.load(f)

# for k,v in map_json.items():
#     print(v["content"]["indicator"]["description"])

def convertLabelContent(jsonOutput: str, source: str):
    # DBversion V8
    # "labelID": {
    #     "locale": {
    #         "zh_tw": ""
    #     },
    #     "type": {},
    #     "usage": [],
    #     "region": [],
    #     "typesSpecific": [],
    #     "defaultCategory": "",
    #     "md": 103
    # }

    with open(source, 'r', encoding="utf-8") as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        content = {}

        line_count = 0
        for row in spamreader:
            if line_count == 0:
                print(f'Column names are {",".join(row)}')
            elif row[1] == "":
                print(f'Row {line_count} is empty, Return')
            elif row[1] in shouldRemoveKeyword:
                print(f'{row[1]} should be remove, Return')

            else:

                print(f'{row[1]} is processing')

                content[row[1]] = {
                    "tempID": row[1],
                    "defaultCategory": (row[5].split("<")[1]).split(">")[0],
                    "locale": {
                        "zh_tw": row[7],
                        "zh_hk": row[8],
                        "en_us": row[9],
                    },
                    "usage": stringToID(
                        source=row[6], midSepreator=","),
                    "typesSpecific": stringToID(row[12]),
                    "md": adminTool.timestamp(),
                    "type": "standard",
                    "region": stringToID(row[4])
                }

            line_count += 1
        with open(jsonOutput, 'w', encoding='utf-8') as outfile:
            json.dump(content, outfile, ensure_ascii=False)


def stringToID(source: str, leftSepreator="<", righSepreator=">", midSepreator=None):
    keys: list(str) = list()

    if midSepreator == None:
        line: int = 0

        keyArray: list = source.split(leftSepreator)
        key: str
        for key in keyArray:
            if line == 0:
                pass
            else:
                keys.append((key.split(righSepreator))[0])
            line += 1
    else:

        keyArray = source.split(midSepreator)
        key: str
        for key in keyArray:
            keys.append(key.strip().lower())
    return keys


def convertLabelCat(jsonOutput: str, source: str):
    with open(source, 'r', encoding="utf-8") as csvFile:
        spamreader = csv.reader(csvFile, delimiter=",")
        content: dict = {}
        lineCount = 0

        for row in spamreader:
            if lineCount == 0:
                print(f'Column names are {",".join(row)}')
            elif row[0] == "":
                print(f'Row {lineCount} is empty, Return')

            else:
                catId: str = row[0]
                catType: str = row[1]
                icon: str = row[2]
                label_zh_tw = row[4]
                label_zh_hk = row[5]
                label_en_us = row[6]

                description_zh_tw: str = ""
                description_zh_hk: str = ""
                description_en_us: str = ""

                if(row.__len__() == 7):
                    print(f"{catId} have No description")
                elif (row.__len__() == 8):
                    print("only zh description")
                    description_zh_tw = row[7]

                elif (row.__len__() == 9):
                    print("no eng description")
                    description_zh_tw = row[7]
                    description_zh_hk = row[8]

                elif (row.__len__() == 10):
                    print("full description")
                    description_zh_tw = row[7]
                    description_zh_hk = row[8]
                    description_en_us = row[9]

                elif (row.__len__() < 7):
                    print("unknown status: length < 9")

                else:
                    print("unknown status: pass")
                    pass

                content[catId] = {
                    "type": catType,
                    "icon": icon,
                    "label": {
                        "zh_tw": label_zh_tw,
                        "zh_hk": label_zh_hk,
                        "en_us": label_en_us,
                    },
                    "description": {
                        "zh_tw":
                        description_zh_tw,

                        "zh_hk": description_zh_hk,
                        "en_us": description_en_us,
                    },
                    "initiator": "Scorch.Team",
                    "md": 1580907568251,
                }

            lineCount += 1
    with open(jsonOutput, 'w', encoding='utf-8') as output:
        json.dump(content, output, ensure_ascii=False)


def convertPlaceCat(jsonOutput: str, source: str):
    placeTypeCount: int = 0
    placeTypes: list(str) = list()
    content: dict = {}
    with open(source, 'r', encoding="utf-8") as csvFile:
        spamreader = csv.reader(csvFile, delimiter=",")

        lineCount = 0

        for row in spamreader:
            catName: str = row[0]
            if lineCount == 0:
                print(f'Column names are {",".join(row)}')
            elif catName == "":
                print(f'Row {lineCount} is empty, Return')

            else:
                # catId: str = row[0]
                # icon: str = row[1]
                types: list(str) = list()
                # label_zh_tw = labeled
                # label_zh_hk = row[3]
                # label_en_us = row[4]

                courier = row[5].split(sep="<")
                a: int = 0
                for f in courier:
                    if a > 0:
                        types.insert(0,
                                     f.split(">")[0])
                        placeTypes.insert(0, f.split(">")[0])
                    a += 1
                print(f"processed:{types}")
                placeTypeCount = placeTypeCount + len(types)
                catIcon: str = ""
                label_zh_tw: str = ""
                label_zh_hk: str = ""
                label_en_us: str = ""

                if row[1] != None:
                    catIcon = row[1]
                if row[2] != None:
                    label_zh_tw = row[2]
                if row[3] != None:
                    label_zh_hk = row[3]
                if row[4] != None:
                    label_en_us = row[4]

                content[catName] = {
                    "types": types,
                    "icon": catIcon,
                    "label": {
                        "zh_tw": label_zh_tw,
                        "zh_hk": label_zh_hk,
                        "en_us": label_en_us,
                    },
                    "md": 1580907568251,
                }

            lineCount += 1
            print(f"total Place = {placeTypeCount}")

    with open(jsonOutput, 'w', encoding='utf-8') as output:
        json.dump(content, output, ensure_ascii=False)

    return placeTypes


def placeChecker(source: str, checkList: list):
    fullList: dict = {}
    with open(source, 'r', encoding='utf-8') as sourceList:
        placeTypes = json.load(sourceList)
        contentSet = (placeTypes['content']).keys()
        for k in contentSet:
            fullList[k] = 0

    for k in checkList:
        fullList[k] = fullList[k]+1

    for finalK, finalV in fullList.items():
        if finalV == 0:
            print(f"{finalK} is missing, please check")
        elif finalV > 1:
            print(f"{finalK} is duplicated place check")
        else:
            pass

    print("Place Check Done")


def placeSourceCheck(source: str, checkList: str):
    fullList: dict = {}
    with open(source, 'r', encoding='utf-8') as csvFile:
        spamreader = csv.reader(csvFile, delimiter=",")
        linePos = 0
        for p in spamreader:
            if linePos != 0:
                fullList[p[0]] = 0
            linePos += 1

    print(fullList)

    with open(checkList, 'r', encoding='utf-8') as sourceList:
        placeTypes = json.load(sourceList)
        contentSet = (placeTypes['content']).keys()
        for k in contentSet:
            fullList[k] = fullList[k]+1

    for finalK, finalV in fullList.items():

        if finalV == 0:
            print(f"{finalK} is missing, please check")
        elif finalV > 1:
            print(f"{finalK} is duplicated place check")
        else:
            pass

    # with open(jsonOutput, 'w', encoding='utf-8') as output:
    #     json.dump(content, output, ensure_ascii=False)

    # with open('vp.geojson','r', encoding="utf-8") as viewport:
    #     geoDataSupplementary = json.load(viewport)

    # vpData = dict()
    # vpData = {
    #     'md':1578730409390,
    #     'count':55,
    #     'viewports':{}

    # }

    # with open(jsonOutput,'w',newline='',encoding='utf-8') as csvfile:
    # with open(jsonOutput,'r',newline='',encoding='utf-8') as csvfile:
    #     spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    #     i=0
    #     for row in spamreader:
    #         if(i==0):
    #             i = i+1
    #         else:
    #             vp_en =' '.join(row)
    #             vp_id ='hk_'+('_'.join(row)).casefold()
    #             dataSub = geoDataSupplementary['features'][i-1]
    #             vp_zh = dataSub['properties']['Name']
    #             vp_lat = dataSub['geometry']['coordinates'][1]
    #             vp_lng = dataSub['geometry']['coordinates'][0]
    #             terms = [vp_zh,vp_en]
    #             print('vp_en: ',vp_en, 'vp_id: ', vp_id,'vp_identifier: ', vp_zh, 'vp_lat: ',vp_lat, 'vp_lng: ', vp_lng)
    #             vpData['viewports'][vp_id] = {'locale':{'en':vp_en, 'zh_hk':vp_zh}, 'geometry':{'anchor':[{'lat':vp_lat,'lng':vp_lng}]},'terms':terms}
    #             i= i+1

    # with open('hk_viewport.json', 'w', encoding='utf-8') as outfile:
    #     print(vpData['viewports'])
    #     json.dump(vpData, outfile, ensure_ascii=False)

    # override_content :set = set()
    # override_content.add(place[0])
    # print('The totall values sizes',len(place))

    # for k,v in place.items():
    #     print(v['indicator']['description'])

    # sumTagList = list()
    # tags = set()
    # rCounter = 0

    # for k,v in place.items():
    #     l = v['indicator']['description']
    #     taglist = l.split(',')
    #     for tag in taglist:
    #         ntag = tag.lstrip()
    #         # print('KEY TAG:',ntag)
    #         tags.add(ntag)
    #         sumTagList.append(ntag)

    #     if ('Road' in l or 'Street' in l):
    #         print("Road and Street Detected: ", l)
    #         rCounter = rCounter+1

    # print('tag length:',len(tags))
    # print('road count: ', rCounter)

    # with open('p_data.csv','w', newline='',encoding="utf-8") as csvfile:
    #     fieldnames = ['tagID','tagContent','count' ]
    #     writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    #     writer.writeheader()
    #     i = 0
    #     for k in tags:
    #         count = sumTagList.count(k)
    #         # print("processing: ",k,", with count: ",count,', and id in:',i )
    #         writer.writerow({'tagID':i,'tagContent': k, 'count':count})
    #         i=i+1


def labeledConvertor(source: str, endpoint: str):
    output: dict = dict({'md': 1, 'count': 0, 'data': {}})
    labelMap: dict = dict()
    with open("label.json", 'r', encoding='utf-8') as labels:
        labelMap = json.load(labels)
    with open(source, 'r', encoding='utf-8') as csvFile:
        spamreader = csv.reader(csvFile)
        lineCount: int = 0
        lineCountTemp: int = 0
        lineCountTempID: int = 0
        for row in spamreader:
            if lineCount == 0:
                print(f'column Name = {row[0]}, {row[1]}, {labeled}')
            elif '<temp>' in labeled:
                labelList: set(str) = set()
                lineCountTemp += 1
                innerLineCount = 0
                linelist = labeled.split('<temp>')
                output['data'][row[1]] = labeled
                for l in linelist:
                    labeled: str = l.split('<')[0]
                    labeled = labeled.replace(',', '')
                    labeledId = labelIDLookup(label=labeled, labels=labelMap)
                    if labeledId != None:
                        labelList.add(labeledId)
                    else:
                        pass
                output['data'][row[1]] = list(labelList)
                print(
                    f"OutPut: {row[1]}, Label List: {output['data'][row[1]]}")

            else:
                labelList: set(str) = set()
                lineCountTempID += 1
                linelist = labeled.split('<')
                innerLineCount = 0
                for l in linelist:
                    if innerLineCount != 0:
                        labeledId: str = labelIDLookup(label="", labels=labelMap,
                                                       tempID=l.split('>')[0])
                        if labeledId != None:
                            labelList.add(labeledId)
                        else:
                            pass
                    else:
                        pass

                    innerLineCount += 1
                output['data'][row[1]] = list(labelList)
                print(
                    f"OutPut: {row[1]}, Label List: {output['data'][row[1]]}")
            lineCount += 1
    lenA = len(output['data'])
    output['count'] = lenA
    print(
        f'total line:{lineCount}, total temp Line: {lineCountTemp}, total tempID Line = {lineCountTempID}, actuallen:{lenA}')
    with open(endpoint, 'w', encoding='utf-8') as endfile:
        json.dump(output, endfile, ensure_ascii=False)


def labelGenerator(labeled: str, labels: dict):
    labelList: set(str) = set()
    lineCountTemp = 0
    if '<temp>' in labeled:

        lineCountTemp += 1
        innerLineCount = 0
        linelist = labeled.split('<temp>')
        for l in linelist:
            labeled: str = l.split('<')[0]
            labeled = labeled.replace(',', '')
            labeledId = labelIDLookup(label=labeled, labels=labels)
            if labeledId != None:
                labelList.add(labeledId)
            else:
                pass
        return list(labelList)

    else:
        labelList: set(str) = set()
        linelist = labeled.split('<')
        innerLineCount = 0
        for l in linelist:
            if innerLineCount != 0:
                labeledId: str = labelIDLookup(label="", labels=labels,
                                               tempID=l.split('>')[0])
                if labeledId != None:
                    labelList.add(labeledId)
                else:
                    pass
            else:
                pass
            innerLineCount += 1
        return list(labelList)


def certificateGenerator(labeled: any, issuer: str, labels: dict = None):
    certificate: dict = dict()
    lineCountTemp = 0

    if '<temp>' in labeled:
        lineCountTemp += 1
        innerLineCount = 0
        linelist = labeled.split('<temp>')
        for l in linelist:
            labeled: str = l.split('<')[0]
            labeled = labeled.replace(',', '')
            labeledID = labelIDLookup(label=labeled, labels=labels)
            if labeledID != None:
                certificate[labeledID] = {
                    "scorch": {
                        "extent": None,
                        "issuer": issuer,
                        "md": adminTool.timestamp(),
                        "checker": None}
                }
            elif labeledID in shouldRemoveKeyword:
                print(
                    f"ID {labeledID} should be remove, this line will not process")
            else:
                pass
        return certificate

    elif '<temp' in labeled:
        linelist = labeled.split('<')
        innerLineCount = 0
        for l in linelist:
            if innerLineCount != 0:
                labeledID: str = labelIDLookup(label="", labels=labels,
                                               tempID=l.split('>')[0])
                if labeledID != None:
                    certificate[labeledID] = {
                        "scorch": {
                            "extent": "",
                            "issuer": issuer,
                            "md": adminTool.timestamp(),
                            "checker": None}
                    }

                elif labeledID in shouldRemoveKeyword:
                    print(
                        f"ID {labeledID} should be remove, this line will not process")

                else:
                    pass
            else:
                pass
            innerLineCount += 1
        return certificate
    else:
        for l in labeled:

            if labeledID in shouldRemoveKeyword:
                print(
                    f"ID {labeledID} should be remove, this line will not process")
            else:
                certificate[l] = certificate[labeledID] = {
                    "scorch": {
                        "extent": "",
                        "issuer": issuer,
                        "md": adminTool.timestamp(),
                        "checker": None}
                }
        return certificate


def labelIDLookup(label: str, labels: dict, tempID: str = None):
    for labelK, labelV in labels.items():
        if tempID != None:
            nestedID: str = labelV.get('tempID')
            if nestedID == tempID:
                return labelK
            else:
                pass
        else:
            if label in labelV["label"]['locale'].values():

                return labelK
            else:
                pass


def placeV2ToV3(placeJson: str, outputJson: str, labeledCSV: str):
    if outputJson != None:
        placeV2 = (adminTool.readJson(placeJson))["data"]
    with open(labeledCSV, 'r', encoding='utf-8') as source:
        spamreader = csv.reader(source)
        line = 0
        for row in spamreader:
            if line > 0:
                pK = row[2]
                issuer = row[1]
                place: dict = placeV2.get(pK)
                placeStatus: str = place.get("status"),
                referee: int = place.get("referee")
                counter: dict = dict()
                cert: dict = certificateGenerator(
                    labeled=place["labels"], issuer=issuer)
                for k in cert.keys():
                    if place.get("counter") == None:
                        counter[k] = 100
                    elif place["counter"].get(k) == None:
                        counter[k] = 100
                    else:
                        place["counter"][k] = (place["counter"][k])+1

                if placeStatus == None:
                    place["status"] = "onStage"
                else:
                    pass
                if referee == None:
                    place["referee"] = 0
                else:
                    pass

                place.pop("labels")
                place["counter"] = counter
                place["certificate"] = cert
                placeV2[pK] = place

            else:
                print(f"Column Title: {row}")
            line += 1

    adminTool.writeJson(source=outputJson, data={
        "count": len(placeV2),
        "md": adminTool.timestamp(),
        "data": placeV2})
    print("convert Done")


def placeV1PToV3(dbV1: str, outputJson: str, placeV1P: str):
    if outputJson != None:
        counterV1: dict = adminTool.readJson(
            dbV1)["bundled"]["placeCounter"]["data"]
        placeV1: dict = adminTool.readJson(placeV1P)["data"]
        structureV3: dict = dict()
        line = 0
        for pK, pV in placeV1.items():
            oCounter: dict = counterV1.get(pK)

            if(oCounter == None):
                print(f"{pK} return Null when calling oCunter")
                pV["counter"] = None
                pV["certificate"] = None
            else:
                oCounter.pop("ref")
                oCounter.pop("referee")
                counter: dict = dict()
                referee: int = pV.get("referee")
                cert: dict = certificateGenerator(
                    labeled=oCounter.keys(), issuer="map.yelloNblue")
                for k in cert.keys():
                    if pV.get("counter") == None:
                        counter[k] = 100
                    elif pV["counter"].get(k) == None:
                        counter[k] = 100
                    else:
                        pV["counter"][k] = (pV["counter"][k])+1
                else:
                    pass
                pV["counter"] = counter
                pV["certificate"] = cert

            referee: int = pV.get("referee")
            if referee == None:
                pV["referee"] = 0
            structureV3[pK] = pV
            line += 1

    adminTool.writeJson(source=outputJson, data={
        "count": len(structureV3),
        "md": adminTool.timestamp(),
        "data": structureV3})
    print("convert Done")


def jsonCombiner(primary: str, secondary: str, output: str):
    jsonPrimary: dict = adminTool.readJson(primary)["data"]
    jsonSecondary: dict = adminTool.readJson(secondary)["data"]

    for uk, uV in jsonSecondary.items():
        root: dict = jsonPrimary.get(uk)
        if root != None:
            primaryCert: dict = root.get("certificate")
            primaryCounter: dict = root.get("counter")
            secondaryCert: dict = uV.get("certificate")
            secondaryCounter: dict = uV.get("counter")
            newCert: dict = secondaryCert.update(primaryCert)
            newCounter: dict = secondaryCounter.update(primaryCounter)
            uV["certificate"]["scorch"] = newCert
            uV["counter"] = newCounter
            jsonPrimary[uk] = uV
            print(
                f"{uk} duplicated, replace by Secondary")
        else:
            newCert: dict = uV["certificate"]
            uV.pop("certificate")
            uV["certificate"] = {"scorch": newCert}
            jsonPrimary[uk] = uV
            # print(f"{uk} new Content Detected")

    adminTool.writeJson(source=output, data={
        "md": adminTool.timestamp(),
        "data": jsonPrimary,
        "count": len(jsonPrimary)

    })
    print("All Converted")


def userRestructure():
    userJson: dict = adminTool.readJson("old/user.json")
    count = len(userJson)
    line = 1
    for uK, uV in userJson.items():
        userType = uV.get("userType")
        userReferenceId = uV.get("userReferenceId")
        labelCounter: dict = uV.get("labelCounter")
        placeCounter: dict = uV.get("placeCounter")

        userJson[uK] = {"private": {
            "label": None, "labelCounter": labelCounter, "labelCategory": None, "placeCounter": placeCounter, "favorited": None
        },
            "preference": {"language": "zh_hk", "updateFequency": 3600},
            "profile": {"region": "hkg", "referenceId": userReferenceId, "userType": userType, "refered": 0},
            "billing": {
            "isAutoRenewing": None,
            "md": None,
            "expiry": None,
            "subscription": None
        }
        }
        print(f"{uK} restructured, {line} / {count}")
        line += 1

    adminTool.writeJson(source="updates/userV4.json", data=userJson)


def restructureV4TOV5(source: str, output: str):

    jsonSource: dict = adminTool.readJson(source)
    data: dict = jsonSource["data"]
    count: int = jsonSource["count"]

    def peroidConverter(opening_hour: list):
        newOpeningHour: dict = dict()
        # 0 = sun..
        # 1 = mon..
        hour: dict
        for hour in opening_hour:
            replacement: dict = dict()
            openT: dict = hour.get("open")
            closeT: dict = hour.get("close")
            day: int = openT.get("day")
            tK: str = str()
            to: int = int(int(openT.get("time"))/100*3600)
            tc: int = 0
            td: int = 0
            if closeT == None:
                pass
                td = 86400
            else:
                tc = int(int(closeT.get("time"))/100*3600)
                td = int(tc-to)

            if day == 0:
                tK = "sun"
            elif day == 1:
                tK = "mon"
            elif day == 2:
                tK = "tue"
            elif day == 3:
                tK = "wed"
            elif day == 4:
                tK = "thur"
            elif day == 5:
                tK = "fri"
            elif day == 6:
                tK = "sat"
            else:
                print(f"Unreaderble Day Detected, {day}")

            replacement = {"duration": td, "open": to, "close": tc}
            original: list = newOpeningHour.get(tK)
            if original == None:
                newOpeningHour[tK] = [replacement]
            else:
                original.insert(1, replacement)
                newOpeningHour[tK] = original

        return newOpeningHour

    def readableConvertor(opening_hour: list):
        readable: dict = dict()
        line: str
        for line in opening_hour:
            trimContent: str
            k: str
            if "Monday:" in line:
                trimContent = "Monday: "
                k = "mon"
            elif "Tuesday:" in line:
                trimContent = "Tuesday:"
                k = "tue"
            elif "Wednesday:" in line:
                trimContent = "Wednesday: "
                k = "mon"
            elif "Thursday:" in line:
                trimContent = "Thursday: "
                k = "thur"
            elif "Friday:" in line:
                trimContent = "Friday: "
                k = "fri"
            elif "Saturday:" in line:
                trimContent = "Saturday: "
                k = "sat"
            elif "Sunday:" in line:
                trimContent = "Sunday: "
                k = "sun"
            else:
                print(f"unreaderbale line: {line}")

            lines: list(str) = line.split(trimContent)
            readline = (lines[1]).lstrip()
            readable[k] = readline
        return readable

    lineCount = 1
    for dK, dV in data.items():
        supcontent: dict = adminTool.getPlaceDetail(dK)
        if supcontent == None:
            print(
                f"{dK} may be deleted from the source, no info detected")
            pass
        elif supcontent.get("opening_hours") != None:
            print(f"{dK} Converting  , in {lineCount}/ {count}")
            reformHR: dict = {
                "periods": peroidConverter(opening_hour=supcontent["opening_hours"]["periods"]),
                "readable": readableConvertor(supcontent["opening_hours"]["weekday_text"])
            }
            dV["opening_hours"] = reformHR
            data[dK] = dV
        else:
            print(f"{dK} may have no opening hour")
        lineCount += 1
    adminTool.writeJson(source=output, data={
                        "count": len(data),
                        "md": adminTool.timestamp(), "data": data})


def restructureV6(source: str, output: str):
    # opening hour to detail
    placeV5: dict = adminTool.readJson(source)["data"]
    count = len(placeV5)
    def convert(original: dict):

        original["indicator"]["types"] = original["types"]
        original["indicator"]["geometry"] = original["geometry"]
        details: dict = original.get("details")

        details["opening_hours"] = original.get("opening_hours")
        details["terms"] = (original["indicator"]).get("terms")
        details["description"] = (original["indicator"]).get("description")
        details["secondaryDescription"] = (
            original["indicator"]).get("secondaryDescription")

        original.pop("opening_hours")
        (original["indicator"]).pop("terms")
        (original["indicator"]).pop("description")
        (original["indicator"]).pop("secondaryDescription")
        original.pop("types")
        original.pop("geometry")
        counter: dict = original["counter"]

        addRef: bool = False
        if counter != None:
            for cK in counter.keys():
                if "bundle" in cK:
                    addRef = True
                else:
                    pass

            if addRef == True:
                original["referee"] = 1
            else:
                pass

        original["details"] = details
        original["counter"] = counter
        return original
    line = 1
    for oK, oV in placeV5.items():
        print(f"handling {oK}, {line}/{count}")
        placeV6: dict = convert(original=oV)
        placeV5[oK] = placeV6

        line += 1
    adminTool.writeJson(source=output, data={
        "md": adminTool.timestamp(),
        "count": len(placeV5),
        "data": placeV5
    })


def labelReplacement(source: str, output: str, replacements: str):
    jsonContent: dict = adminTool.readJson(source)
    data: dict = jsonContent.get("data")
    count: int = jsonContent.get("count")
    res: list = adminTool.readJson(replacements)["data"]

    line = 1
    replacement: dict
    for replacement in res:
        original: str = replacement["original"]
        new: str = replacement["replacement"]
        for pK, pV in data.items():
            print(f"handling: {pK}, {line}/{count}")
            counter: dict = pV.get("counter")
            cert: dict = pV["certificate"].get("scorch")
            if counter != None:
                counterK: list(str) = counter.keys()
                if (original in counterK) & (new in counterK):
                    counter.pop(original)
                    pV["counter"] = counter
                    print(
                        f"{original} and {new} found in {pK} counter, pop {original}")
                elif (original in counterK) & (new not in counterK):
                    newContent: dict = counter[original]
                    counter.pop(original)
                    counter[new] = newContent
                    pV["counter"] = counter
                    print(f"{original} found in {pK} counter, replace {original}")
                elif (original not in counterK) & (new in counterK):
                    pass
                else:
                    pass
            if cert != None:
                certK: list(str) = cert.keys()
                if (original in certK) & (new in certK):
                    cert.pop(original)
                    pV["certificate"] = {"scorch": cert}
                    print(
                        f"{original} and {new} found in {pK} cert, pop {original}")
                elif (original in certK) & (new not in certK):
                    newContent: dict = cert[original]
                    cert[new] = newContent
                    cert.pop(original)
                    pV["certificate"] = {"scorch": cert}
                    print(f"{original} found in {pK} cert, replace {original}")
                elif (original not in certK) & (new in certK):
                    pass

                else:
                    pass
            else:
                print(f"{pK} have No cert")

            data[pK] = pV
            line += 1

    adminTool.writeJson(source=output, data=data)


def certRestructure(source: str, output: str):
    jsonContent: dict = adminTool.readJson(source)
    data: dict = jsonContent["data"]
    count: int = jsonContent["count"]
    line = 1
    for cK, cV in data.items():
        cert: dict = cV.get("certificate")
        if cert != None:
            scorchCert: dict = cert.get("scorch")
            if (scorchCert == None) & (len(cert.keys()) > 1):

                scorchCert = dict.copy(cert)
                if scorchCert.get("scorch") != None:
                    scorchCert.pop("scorch")
                print(
                    f"{cK} is mis-repersenting, correcting to scorch: {scorchCert.keys()}, {line}/{count}")
                cV["certificate"] = {"scorch": scorchCert}
                data[cK] = cV
            else:
                pass
        line += 1

    adminTool.writeJson(source=output, data=data)


def placeV6DurationRestructure(source: str, output: str):
    jsonContent = adminTool.readJson(source)
    data: dict = jsonContent["data"]

    for pK, pV in data.items():
        openingHour: dict = pV["details"].get("opening_hours")
        if openingHour != None:
            peroids: dict = openingHour.get("periods")
            dV: dict
            for dK, dV in peroids.items():
                # dk : sun....
                line = 0
                for duration in dV:
                    if duration["duration"] < 0:
                        newDur: int = int(
                            3600*24 - duration["open"]+duration["close"])
                        duration["duration"] = newDur
                        dV[line] = duration
                        print(
                            f"negetive duration detected in {pK} & {dK}, return a new dur: {duration}")
                    else:
                        pass
                    line = +1
                peroids[dK] = dV
                pV["periods"] = peroids

            data[pK] = pV
        else:
            print(f'{pK} have no opening hour')
            pass
    adminTool.writeJson(source=output, data=data)


def placeTypev3(source: str, output: str):
    jsonContent: dict = adminTool.readJson(source)["data"]
    tV: dict
    for tK, tV in jsonContent.items():
        zhContent: str = tV.get("displayZH")
        enContent: str = tV.get("displayEN")
        icon: str = tV.get("icon")
        jsonContent[tK] = {"label": {"zh_tw": zhContent,
                                     "en": enContent, "zh_hk": zhContent}, "icon": icon}

    adminTool.writeJson(source=output, data=jsonContent)


def labelV3ToV4(source: str, output: str):
    data = adminTool.readJson(source)["data"]
    line: int = 1
    for lK, lV in data.items():
        label: dict = lV["label"].get("locale")
        labelCat: str = lV["labelCat"]
        lV.pop("labelCat")
        lV["label"] = label
        lV["taggable"] = True
        lV["category"] = labelCat
        data[lK] = lV
        print(f"{lK} converted {line} / {len(data)}")
        line += 1

    adminTool.writeJson(source=output, data=data)


def placeV6toV7(source: str, output: str):
    data = adminTool.readJson(source)["data"]

    adminTool.writeJson(source=output, data=data)


def timeToPeriordic(time: list):

    perioridic: list(dict) = list()

    def getTimeDelta(timeStr: str):
        time_only = dtt.strptime(timeStr, "%H%M") - \
            dtt.strptime("00:00", "%H:%M")
        return int(time_only.total_seconds())
    t: dict
    i = 0
    for t in time:

        closeDay: int = (t["close"]["day"])*86400
        closeTime: int = getTimeDelta(
            t["close"]["time"])
        openDay: int = (t["open"]["day"])*86400
        openTime: int = getTimeDelta(t["open"]["time"])
        duration: int
        if closeTime > openTime:
            duration = closeTime - openTime
        elif closeTime == openTime:
            duration = 86400
        else:
            duration = closeTime + (86400 - openTime)

        if (openDay+openTime+duration) > 604800:
            remaining: int = (openDay+openTime+duration) - 604800

            perioridic.extend([
                {
                    "open": openDay+openTime,
                    "duration": duration-remaining,
                    "close": None
                }, {
                    "open": None,
                    "close": remaining,
                    "duration": remaining

                }]
            )

        else:
            perioridic.append({
                "open": openDay+openTime,
                "close": closeDay+closeTime,
                "duration": duration,
            })
        i += 1

    return perioridic
